(function() {
    angular
        .module("WMS")
        .controller("RegCtrl", RegCtrl);
    RegCtrl.$inject = ['$window', 'WedService'];


    function RegCtrl($window, WedService) {


        var vm = this;

        vm.guests = {
            email: "",
            name: ""
        };
        vm.status = "";

        vm.register = register;



        function register() {

            //alert("The registration information you sent are \n" + JSON.stringify(vm.guests));

            console.log("The registration information you sent were:");
            console.log("Guest Email: " + vm.guests.email);
            console.log("Guest name: " + vm.guests.name);


            WedService
                .insertGuest(vm.guests)
                .then(function(result) {
                    console.log("result " + JSON.stringify(result));
                   // $window.location.assign('/app/registrationGuest/thanks.html');
                   vm.status = "Thank you for accpeting the invite";
                   vm.guests.email = "";
                   vm.guests.name = "";

                })
                .catch(function(err) {
                    console.log("Ctrlerror " + err);
                    vm.status = "Something went wrong, please try again";
                });
        } // END function register()
    } // END RegCtrl

})();
(function() {
    angular
        .module("WMS")
        .controller("ListGuestCtrl", ListGuestCtrl);

    ListGuestCtrl.$inject = ['$window', 'WedService'];


    function ListGuestCtrl($window, WedService) {


        var vm = this;

        vm.guestList = [];
        vm.list = list;
        vm.attendance = attendance;
        vm.deleteGuest = deleteGuest;


        function list() {
            console.log("sending from ctrl");
            WedService
                .retrieveGuest()
                .then(function(result) {
                    //result.data = {guest_email:'abc@abc.com',guest_name:'abc',tableNo:'1',attendance:true}
                    vm.guestList = result.data;
                    console.log("result " + JSON.stringify(result));

                })
                .catch(function(err) {
                    console.log("Ctrlerror " + err);
                });
        } // END function list()

        function attendance(guest_email) {
            console.log("sending from ctrl mark attendance");

            var attendance = true;

            WedService
                .markAttendance(guest_email, attendance)

            .then(function(result) {
                    list();
                    console.log("result " + JSON.stringify(result));

                })
                .catch(function(err) {
                    console.log("Ctrlerror " + err);
                });
        } // END function list()

        function deleteGuest(guest_email) {
            console.log("sending from ctrl delete attendance");
            console.log(guest_email);
            WedService
                .deleteGuest(guest_email)

            .then(function(result) {
                    list();
                    console.log("result " + JSON.stringify(result));

                })
                .catch(function(err) {
                    console.log("Ctrlerror " + err);
                });
        } // END function list()
    } // END ListGuestCtrl

})();
(function(){
    angular
        .module("WMS")
        .controller("RegisterCtrl",RegisterCtrl);

RegisterCtrl.$inject = ["$state","WedService"];

function RegisterCtrl($state,WedService){
    var vm = this;

    vm.user  = { 
                email:"",
                usernames:"",  
                passwords:"", 
                user_type:"guest"
            };
            
    vm.status = "";
    vm.registerUser = registerUser;

    function registerUser(){
        console.log("registerUser.....");
        WedService
            .register(vm.user)
            .then(function(result){
                vm.status = "Registered Sucessfull, Please login !";
                console.log("result " + JSON.stringify(result));
                //$window.location.assign('/app/gallery/gallery.html');
                $state.go("gallery");
                
            })
            .catch(function(err){
                vm.status = "Something went wrong, Please try again";
            });
        } //end of registerUser()
    } //end of RegisterCtrl()
    

})();